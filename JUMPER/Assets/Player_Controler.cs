﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controler : MonoBehaviour
{
    public int walkspeed;
    public bool isJump;
    private int jumpHigh;
    public double x;
    public double y;


    // Start is called before the first frame update\ 

    void Start()
    {
        walkspeed = 5;
        isJump = false;
        jumpHigh = 10;


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isJump = true;
            y = y + jumpHigh;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            x = x + walkspeed;
            

        }
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            x = x - walkspeed;

        }
    }
}
